import json
import urllib
import requests
import os
import time


def getValuesForProp(currProperty,props,isText):
	vals = prop[currProperty]['values']
	allValues = list()
	for rslt1 in vals:
		if isText == 'true':
			allValues.append(rslt1['text'])
		else:
			allValues.append(rslt1['values'])
	return allValues
def getAttrValues(filmProps,filmAttrs,eachWordInfo):
	# print filmProps
	for attr in filmAttrs:
		eachWordInfo['attributes'][attr.split("/")[3].replace("_"," ")] = list()
	for prop in filmProps:
		for attr in filmAttrs:
			# print prop['master_property'] +"  &&   "+ attr
			if prop['master_property'] == attr:
				print 'here'
				key = attr.split("/")[3].replace("_"," ")
				(eachWordInfo['attributes'][key]).append(prop['target'])
	# print eachWordInfo
service_url = 'https://www.googleapis.com/freebase/v1/topic'
topic_id = '/m/0d6lp'
params = {
  'key': 'AIzaSyAJ-I0RHEHfBQY54vSYp-0znLjorB6y5NE',
  'filter': '/common/topic'
}


query = json.dumps( [{'id': None, 'mid':None,'name': None,'type': '/film/film'}])
topic = requests.get('https://www.googleapis.com/freebase/v1/mqlread?query=%s' % query )
i=0
collection = list()
for rslt in topic.json()['result']:
	time.sleep(10)
	name = rslt['name']
	synList = list()

	collection.append(name)
	eachWordInfo = dict()
	print rslt['name'] + " &&& " + rslt['mid']
	mid = rslt['mid']
	eachWordInfo['word'] = name
	eachWordInfo['category'] = 'film'
	eachWordInfo['attributes'] = dict()
	query1 = json.dumps( [{"master_property": None,"source": {"mid":mid},"target": None,'target_value': None,"type": "/type/link"}])
	filmProps = requests.get('https://www.googleapis.com/freebase/v1/mqlread?query=%s' % query1 )
	if 'result' in filmProps.json().keys() and len(filmProps.json()['result']) > 0:
		filmAttrs = ['/film/film/country','/film/film/language','/film/film/music','/film/film/sequel','/film/film/cinematography','/film/film/genre',
		'/film/film/costume_design_by','/film/film/rating','/film/film/film_art_direction_by','/film/film/executive_produced_by','/film/film/edited_by','/film/film/film_casting_director','/film/film/film_production_design_by','/film/film/film_set_decoration_by','/film/film/film_festivals']
		
		getAttrValues(filmProps.json()['result'],filmAttrs,eachWordInfo)
		
		
		eachWordInfo['attributes']['taglines'] = list()
		for prop in filmProps.json()['result']:
		
			if prop['master_property'] == '/film/film/tagline':
				key = 'taglines'
				(eachWordInfo['attributes'][key]).append(prop['target_value'])
	else:
		print filmProps.json()
	
	url = service_url + mid + '?' + urllib.urlencode(params)
	prop = json.loads(urllib.urlopen(url).read())['property']
	
	# print prop
	if '/common/topic/description' in prop.keys():
		data = prop['/common/topic/description']['values'][0]['value']
		eachWordInfo['description'] = data
	
	eachWordInfo['URL'] = 'https://www.freebase.com'+mid
	if '/common/topic/alias' in prop.keys():
		eachWordInfo['attributes']['SYN'] = getValuesForProp('/common/topic/alias',prop,'true')

	if '/film/film/directed_by' in prop.keys():
		eachWordInfo['attributes']['directed by'] = getValuesForProp('/film/film/directed_by',prop,'true')

	if '/common/topic/notable_for' in prop.keys():
		eachWordInfo['attributes']['notable for'] = getValuesForProp('/common/topic/notable_for',prop,'true')

	if '/film/film/written_by' in prop.keys():
		eachWordInfo['attributes']['written by'] = getValuesForProp('/film/film/written_by',prop,'true')

	if '/film/film/produced_by' in prop.keys():
		eachWordInfo['attributes']['produced by'] = getValuesForProp('/film/film/produced_by',prop,'true')

	if '/common/topic/article' in prop.keys():
		eachWordInfo['attributes']['articles'] = getValuesForProp('/common/topic/article',prop,'true')

	if '/common/document/text' in prop.keys():
		eachWordInfo['attributes']['document description'] = getValuesForProp('common/document/text',prop,'false')

	if '/film/film/initial_release_date' in prop.keys():
		eachWordInfo['attributes']['release date'] = getValuesForProp('/film/film/initial_release_date',prop,'true')

	i+=1
	dirName = "../Data/freebase/films"
	filename = name+"_freebase.json"
	jsonfile = os.path.join(dirName,filename)
	

	import json

	with open(jsonfile, 'w') as outfile:
    		json.dump(eachWordInfo, outfile) 

filmsDict = dict()
filmsDict['film'] = collection

dirName = "../Data"
filename = "films_list.json"
jsonfile = os.path.join(dirName,filename)
import json
with open(jsonfile,'w') as outfile:
	json.dump(filmsDict,outfile)

#print( '------------------------------')



