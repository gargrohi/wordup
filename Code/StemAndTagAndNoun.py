import sys
import os
import tdIdfgenerator
import fnmatch
import json


inputDirPath=''
outputDir=''
if len(sys.argv)>=3:
    inputDirPath=sys.argv[1]
    outputDir=sys.argv[2]
else:
    inputDirPath="../Data"
    outputDir="../Data/Formatted/"

words=dict()


for root,dirNames,fileNames in os.walk(inputDirPath):
    for fileName in fnmatch.filter(fileNames,"*.json"):
        ipath = os.path.join(root,fileName)
        json_data = open(ipath,'r',errors='ignore').read()
        data = json.loads(json_data)
        wordName = data["word"]
        if wordName not in words.keys():
            words[wordName]=[]
        words[wordName].append(ipath)

for key in words.keys():
    try:
        formattedFiles=[]
        mapping=dict()

        for ipath in words[key]:
            formattedFiles.append(os.path.join(outputDir,os.path.basename(ipath)+".formatted"))
            stemmedWords=[]
            values=tdIdfgenerator.getStemmedWords(ipath,stemmedWords)
            outputFile=open(formattedFiles[-1],"w")
            print( " ".join(stemmedWords),file=outputFile)
            outputFile.close()
            for k in values.keys():
                mapping[k]=values[k]

        mapFile=os.path.join(outputDir,os.path.basename(key))+".mapping"
        outputFile=open(mapFile,"w")
        print(str(mapping),file=outputFile)
        outputFile.close()

        topicFile=os.path.join(outputDir,os.path.basename(key))+".topics"

        topics=tdIdfgenerator.topicModelling(formattedFiles)
        outputFile=open(topicFile,"w")
        print(str(topics),file=outputFile)
        outputFile.close()

        allSentences=tdIdfgenerator.getAllSentences(words[key])
        allSentences=tdIdfgenerator.getUniqueSentences(allSentences)

        tfIdfVector=tdIdfgenerator.GenerateVector(formattedFiles)
        allSentences=tdIdfgenerator.rankSentences(allSentences,topicFile,mapFile,tfIdfVector)
        rankFile=os.path.join(outputDir,os.path.basename(key))+".rankedSentences"
        outputFile=open(rankFile,"w")
        for line in allSentences:
            print(line,file=outputFile)
        outputFile.close()

    except:
        print("problem with word",key)





