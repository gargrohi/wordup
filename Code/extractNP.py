import pdb
import os
import json
from nltk.parse import stanford
from nltk import Tree

MAX_DEPTH = 3

def extractNP(tree,wordToBeGuessed,depth,describingWords):
	"""extracts noun phrases from the tree"""
	tempNP = []
	for i in range(0,len(tree)):
	
		if(type(tree[i]) == Tree):
			# if(tree[i].label() == 'NP' and wordToBeGuessed not in tree[i].leaves()):
			# 	tempNP = tree[i].leaves()
			# 	#pdb.set_trace()
			# else:
			# 	extractNP(tree[i],wordToBeGuessed,depth+1,describingWords)
			#pdb.set_trace()
			if(len(tree[i]) > 1):
				if(containsNP(tree[i])):
					extractNP(tree[i],wordToBeGuessed,depth+1,describingWords)
				elif(wordToBeGuessed not in tree[i].leaves()):
					tempNP = tree[i].leaves()
					#pdb.set_trace()
			elif(containsNP(tree[i])):
				#pdb.set_trace()
				extractNP(tree[i],wordToBeGuessed,depth+1,describingWords)
			elif(tree[i].label() == 'NP' and wordToBeGuessed not in tree[i].leaves()):
					tempNP = tree[i].leaves()
			#pdb.set_trace()
		#pdb.set_trace()
		if(len(tempNP) >= 1  and tempNP not in describingWords):
			#pdb.set_trace()
			describingWords.append(tempNP)


def containsNP(tree):
	for i in range(0,len(tree)):
		if(type(tree[i]) == Tree):
			if(tree[i].label() == 'NP'):
				return True
			elif(len(tree[i]) > 1):
				return containsNP(tree[i])


def init(path,fileName):
	MIN_SENTENCE_LENGTH = 2
	os.environ['STANFORD_PARSER'] = '/media/akhilesh/Akhilesh/Study/USCAssignments/NLP/project/stanford-parser-full-2015-01-30'
	os.environ['STANFORD_MODELS'] = '/media/akhilesh/Akhilesh/Study/USCAssignments/NLP/project/stanford-parser-full-2015-01-30'

	parser = stanford.StanfordParser(model_path='/media/akhilesh/Akhilesh/Study/USCAssignments/NLP/project/stanford-parser-full-2015-01-30/stanford-parser-3.5.1-models/edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz')
	#os.chdir('../Data/Formatted')

	#pdb.set_trace()
	try:
		rsFile = open(path+ '/' +fileName)
		
		rsData = rsFile.read()

		wordToBeGuessed = fileName.split('.')[0]
		description = rsData.split('\n')
		
		describingWords = []
		for sentence in description:
			#print(sentence)
			sentenceNP = []
			if(len(sentence) > MIN_SENTENCE_LENGTH):
				if(sentence[-1] != '.'):
					sentence += '.'
				tree = parser.raw_parse(sentence.strip('\n'))
				#pdb.set_trace()
				for i in range(0,len(tree[0][0])):
					if(len(tree[0][0][i]) > 1):
						sentenceNP.append(extractNP(tree[0][0][i],wordToBeGuessed,0,sentenceNP))
					elif(containsNP(tree[0][0][i])):
						if(wordToBeGuessed not in tree[0][0][i][0].leaves()):
							sentenceNP.append(tree[0][0][i][0].leaves())
			#print(describingWords)
			sentenceNP = [x for x in sentenceNP if x is not None]
			describingWords.append(sentenceNP)
		#print(describingWords)
		return describingWords
	except:
		print("error in reading files")
		return None

if __name__ == '__main__':
	main()