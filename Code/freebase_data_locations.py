import json
import urllib
import requests
import os
service_url = 'https://www.googleapis.com/freebase/v1/topic'
topic_id = '/m/0d6lp'
params = {
  'key': 'AIzaSyB8GpL8gwAlHLo9YIO4_ZDx2-PrqdIH1BA',
  'filter': '/common/topic'
}


query = json.dumps( [{'id': None, 'mid':None,'name': None,'type': '/location/location'}])
topic = requests.get('https://www.googleapis.com/freebase/v1/mqlread?query=%s' % query )
i=0
collection = list()
for rslt in topic.json()['result']:
	name = rslt['name']
	synList = list()

	collection.append(name)
	eachWordInfo = dict()
	print rslt['name'] + " &&& " + rslt['mid']
	mid = rslt['mid']
	eachWordInfo['word'] = name
	eachWordInfo['category'] = 'location'
	eachWordInfo['attributes'] = dict()
	query1 = json.dumps( [{"master_property": "/location/country/capital","source": None,"target": {"mid": mid},"type": "/type/link"}])
	countryCapitalRslt = requests.get('https://www.googleapis.com/freebase/v1/mqlread?query=%s' % query1 )
	if 'result' in countryCapitalRslt.json().keys() and len(countryCapitalRslt.json()['result']) > 0:
		capitalCountry = countryCapitalRslt.json()['result'][0]['source']
		eachWordInfo['attributes']['capital of'] = capitalCountry
	url = service_url + mid + '?' + urllib.urlencode(params)
	prop = json.loads(urllib.urlopen(url).read())['property']
	data = prop['/common/topic/description']['values'][0]['value']
	eachWordInfo['description'] = data
	
	eachWordInfo['URL'] = 'https://www.freebase.com'+mid
	if '/common/topic/alias' in prop.keys():
		alias = prop['/common/topic/alias']['values'][0]['text']
		synList = alias.split(",")
		eachWordInfo['attributes']['SYN'] = synList
	i+=1
	dirName = "../Data/freebase/locations"
	filename = name+"_freebase.json"
	jsonfile = os.path.join(dirName,filename)
	

	import json

	with open(jsonfile, 'w') as outfile:
    		json.dump(eachWordInfo, outfile) 

locationsDict = dict()
locationsDict['locations'] = collection

dirName = "../Data"
filename = "locations_list.json"
jsonfile = os.path.join(dirName,filename)
import json
with open(jsonfile,'w') as outfile:
	json.dump(locationsDict,outfile)

#print( '------------------------------')




