from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
import collections
import math
import os
import ast
import nltk
from nltk import pos_tag
from nltk.stem.snowball import SnowballStemmer
import json
from nltk import tokenize
from requests import get

def GenerateVector(fileNames):
    documents=[]
    for file in fileNames:
        document=open(file,'r',errors='ignore').read()
        documents.append(document)

    test_v = TfidfVectorizer()    ### applied the model
    t = test_v.fit_transform(documents)


    row=-1
    distinctFeatures=dict()
    for file in fileNames:
        row+=1
        col=-1
        for w in test_v.get_feature_names():
            col+=1
            distinctFeatures[w]=t._get_single_element(row,col)

    return distinctFeatures


def totalidIdf(tfIdfVector,d2): #d2 is query document

    Total_TF_IDF=0

    for item in d2.split():
        if item in tfIdfVector.keys():
            Total_TF_IDF+=tfIdfVector[item]

    #print(Total_TF_IDF)
    return Total_TF_IDF


def min(a,b):
    if(a>b):
        return b
    return a


def cosineContentBased(document1, document2):

    document1=document1.lower()
    document2=document2.lower()

    doc1Dict= collections.Counter()
    doc2Dict=collections.Counter()

    for w in document1.split():
        doc1Dict[w] += 1

    for w in document2.split():
        doc2Dict[w] += 1

    cosum=0
    for k in doc1Dict.keys():
        if k in doc2Dict.keys():
            cosum+= doc1Dict[k]*doc2Dict[k]

    squares1=0
    for k in doc1Dict.keys():
        squares1+=(float(doc1Dict[k])*float(doc1Dict[k]))

    if(squares1==0):
        squares1=1

    squares2=0
    for k in doc2Dict.keys():
        squares2+=(float(doc2Dict[k])*float(doc2Dict[k]))

    if(squares2==0):
        squares2=1


    cosine=cosum/(math.sqrt(squares1)*math.sqrt(squares2))
    return cosine



def getStemmedWords(ipath,stemmedWords):
    stemmed_words=dict()
    stemmer = SnowballStemmer("english")
    description=""
    json_data = open(ipath,'r',errors='ignore').read()
    data = json.loads(json_data)
    if data.get("description") :
        description= str(data["description"]) +" "


    tokens = description.strip().split() # Generate list of tokens
    tokens_pos = pos_tag(tokens)
    for token in tokens_pos:
        if(token[1].startswith("NN") or token[1].startswith("JJ")):
            stemmed_words[token[0]] = stemmer.stem(token[0])
            stemmedWords.append(stemmer.stem(token[0]))

    return stemmed_words

def getUniqueSentences(allSentences):
    uniqueSentences=[]

    for outerIdx,line in enumerate(allSentences):
        duplicate=0

        for unique in uniqueSentences:
            if(cosineContentBased(line,unique)>0.75):
                duplicate=1

        if duplicate==0:
            uniqueSentences.append(line)

    return uniqueSentences

def rankSentences(sentences,rankFile,mappedFile,tfIdfVector):

    rankDic= open(rankFile,'r',errors='ignore').read()
    rankDic=ast.literal_eval(rankDic)

    mapDic=open(mappedFile,'r',errors='ignore').read()
    mapDic=ast.literal_eval(mapDic)

    sentenceRank=dict()
    for line in sentences:
        sum=0
        mainWords=[]
        for word in line.strip().split():
            if word in mapDic.keys():
                if mapDic[word] in rankDic.keys():
                    mainWords.append(word)

        sentenceRank[line]=totalidIdf(tfIdfVector," ".join(mainWords))

        ranks=[]
    for w in sorted(sentenceRank, key=sentenceRank.get, reverse=True):
        ranks.append(w)

    return ranks

def getAllSentences(files):
    allSentences=[]
    description=""
    for ipath in files:
        json_data = open(ipath,'r',errors='ignore').read()
        data = json.loads(json_data)
        if data.get("description"):
            description= str(data["description"])
        for line in tokenize.sent_tokenize(description.strip()):
            allSentences.append(line)

    return allSentences

def topicModelling(formattedFiles):

    num_words = 6
    documents=[] 
    main_words=dict()
    for ipath in formattedFiles:
        data=open(ipath,'r',errors='ignore').read().strip()
        if data !="":
            documents.append(data)

    num_clusters = min(len(documents),3)
    vectorizer = TfidfVectorizer(stop_words='english')
    X = vectorizer.fit_transform(documents)
    model = KMeans(n_clusters=num_clusters , init='k-means++', max_iter=100, n_init=1)
    model.fit(X)

    labels=model.labels_.tolist()
    order_centroids = model.cluster_centers_.argsort()[:, ::-1]
    terms = vectorizer.get_feature_names()
    for i in range(num_clusters):
        for ind in order_centroids[i, :num_words]:
            if terms[ind] not in main_words:
                main_words[terms[ind]]=1
            else:
                main_words[terms[ind]]+=1

    return  main_words      
            
    




def getPhraseSimilarity(s1, s2, type='concept', corpus='webbase'):
    try:
        sss_url = "http://swoogle.umbc.edu/SimService/GetSimilarity"
        response = get(sss_url, params={'operation':'api','phrase1':s1,'phrase2':s2,'type':type,'corpus':corpus})
        return float(response.text.strip())
    except:
        print ('Error in getting similarity for %s: %s' % ((s1,s2), response))
        return 0.0







