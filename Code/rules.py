import nltk
from nltk import Tree
from nltk.corpus import treebank
from collections import Counter
import os
import pdb


def extractRules(node,rules):
	leftLabel = ''
	if(type(node) == list):
		print(node)
		pdb.set_trace()
	if('-' in node.label()):
		leftLabel += node.label().split('-')[0]
	else:
		leftLabel += node.label()
	rightLabel = ''
	for i in range(0,len(node)):
		try:
			if('-' in node[i].label()):
				rightLabel += node[i].label().split('-')[0] + ' '
			else:
				rightLabel += node[i].label() + ' '
		except:
			#not a tree
			pass
	if(len(rightLabel) > 1):
		#print(leftLabel + ' ' + rightLabel)
		rightLabel = rightLabel[:-1] # to remove the extra space added at the end by the above for loop
		rules[leftLabel +' ' + rightLabel] += 1
	for i in range(0,len(node)):
		if(type(node[i]) ==  Tree):
			extractRules(node[i],rules)

def main():

	rules = Counter();
	for i in range(1,199):
		extractRules(treebank.parsed_sents('wsj_'+'%04d' % i +'.mrg')[0],rules)
	os.chdir('MASC-1.0.3/original-annotations/Penn_Treebank')
	fileList = os.listdir(os.getcwd())
	for f in fileList:
		currFile = open(f)
		trees = currFile.read()
		trees = trees.split('\n\n')
		for tree in trees:
			tree = Tree.fromstring(tree)
			extractRules(tree,rules)
	os.chdir('../../../')
	outFile = open('rules.txt','w')
	for key in rules.keys():
		outFile.write(key+'->'+str(rules[key]) + '\n')
	#print(rules)
if __name__ == '__main__':
	main()