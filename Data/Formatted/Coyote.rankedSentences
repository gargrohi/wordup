small wolf native to western North America
It occurs as far north as Alaska and all but the northernmost portions of Canada.
The coyote, also known as the American jackal or the prairie wolf, is a species of canine found throughout North and Central America, ranging from Panama in the south, north through Mexico, the United States and Canada.
It is a highly versatile species, whose range has expanded amidst human environmental modification.
The coyote is a canid native to North America.
The ancestors of the coyote diverged from those of the gray wolf, 1-2 million years ago, with the modern species arising in North America during the Middle Pleistocene.
It is highly flexible in social organization, living either in nuclear families or in loosely-knit packs of unrelated individuals.
This expansion is ongoing, and it may one day reach South America, as shown by the animal's presence beyond the Panama Canal in 2013.
It is listed as "least concern" by the IUCN, on account of its wide distribution and abundance throughout North America, Mexico and into Central America.
There are currently 19 recognized subspecies, with 16 in Canada, Mexico and the United States, and 3 in Central America.
As of 2005, 19 subspecies are recognized.
It has a varied diet consisting primarily of animal matter, including ungulates, lagomorphs, rodents, birds, reptiles, amphibians, fish and invertebrates, though it may also eat fruit and vegetable matter on occasion.
It is a smaller, more basal animal than its close relative, the gray wolf, being roughly the North American equivalent to the old world golden jackal, though it is larger and more predatory in nature.
