any of various tailless stout-bodied amphibians with long hind limbs for leaping; semiaquatic and terrestrial species
They are also one of the five most diverse vertebrate orders.
Frogs are amphibians in the order Anura (meaning "tailless", from Greek an-, without + oura, tail), formerly referred to as Salientia (Latin salere, "to jump").
Besides living in fresh water and on dry land, the adults of some species are adapted for living underground or in trees.
The oldest fossil "proto-frog" appeared in the early Triassic of Madagascar, but molecular clock dating suggests their origins may extend further back to the Permian, 265 million years ago.
Most frogs are characterized by a short body, webbed digits (fingers or toes), protruding eyes, bifid tongue and the absence of a tail.
Frogs are a diverse and largely carnivorous group of short-bodied, tailless amphibians composing the order Anura.
Frogs are widely known as exceptional jumpers, and many of the anatomical characteristics of frogs, particularly their long, powerful legs, are adaptations to improve jumping performance.
The body plan of an adult frog is generally characterized by a stout body, protruding eyes, cleft tongue, limbs folded underneath and the absence of a tail in adults.
The skin of the frog is glandular, with secretions ranging from distasteful to toxic.
Frogs are widely distributed, ranging from the tropics to subarctic regions, but the greatest concentration of species diversity is found in tropical rainforests.
There are approximately 4,800 recorded species, accounting for over 85% of extant amphibian species.
